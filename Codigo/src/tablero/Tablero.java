package tablero;

import casilla.Casilla;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import piezas.Caballo;
import java.io.Serializable;

public class Tablero implements Observer, Serializable{
   private Integer dimension;
   
  public Tablero(Integer dimension){
      if(dimension%2 == 0 && dimension > 8 && dimension <= 26)
          this.dimension = dimension;
      else
          this.dimension = 8;
  }
   private LinkedHashSet<Casilla> crearTablero(Integer dimension){
       Set<Casilla> casillastmp = new LinkedHashSet();
       for(Integer fila = dimension; fila >= 1; fila--)
           for(Integer columna = 97; columna < (97+dimension); columna++)
                casillastmp.add(new Casilla(((char)columna.intValue()), fila));
       return  (LinkedHashSet<Casilla>) casillastmp;
   }
   
   public Boolean incrementarDimension(Integer incremento){
       if(this.dimension > 0 && (this.dimension + (incremento * 2)) <= 26){
           this.dimension += (incremento * 2);
           return true;
       }else
           return false;
   }
   public Boolean existeCasilla(Casilla casilla){
       return crearTablero(this.dimension).contains(casilla);
   }
   
   public Iterator getIterador(){
       return crearTablero(this.dimension).iterator();
   }
   public Boolean decrementarDimension(Integer decremento,Casilla casillaActual){
       if(decremento > 0 && (this.dimension - (decremento * 2)) >= 8){
           Integer fila = casillaActual.getFila();
           Integer columna = casillaActual.getColumna();
           if(fila.equals(this.dimension) || fila.equals(decremento) ||columna.equals(96+this.dimension) || columna.equals(96+decremento) ){
               return false;
           }else{
               this.dimension -= (decremento * 2); 
               return true;
           } 
           
       }else
           return false;
   }
   
    public Integer tamaño(){
       return dimension;
   }
   
    public  Boolean buscarPieza(Set<Casilla> contorno){ //Este es el de buscar pieza 
       Iterator iterador = contorno.iterator();
       while(iterador.hasNext()){
           if(((Casilla) iterador.next()).getPieza() instanceof Caballo){
                return true;
            }
       }
       return false;
   }
   
    @Override
    public String toString() {
        return "Tablero{" + "casillas=" + crearTablero(this.dimension) + ", dimension=" + dimension + '}';
    }

    @Override
    public void update(Observable o, Object arg) {
        Casilla casilla = (Casilla)arg;
        Caballo caballo = (Caballo) o;
        casilla.colocarPieza(caballo);
        //System.out.println("Nuevo Movimiento: -> "+casilla);
    }
}
