package HistorialJuego;
import casilla.Casilla;
import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import java.io.Serializable;

import java.util.Stack;

public class Partida implements Serializable{
    private Stack<Movimiento> historialPartida;
    public Partida() {
        this.historialPartida = new Stack();
    }
    
     public void agregar(Movimiento movimiento){
        historialPartida.push(movimiento);
    }
     
     public void guardar() throws IOException{
        String ruta = JOptionPane.showInputDialog("Nombre del archivo:") + ".txt";
        File archivo = new File(ruta);
        FileOutputStream salida = new FileOutputStream(archivo);
        try(ObjectOutputStream obj = new ObjectOutputStream(salida)){
            obj.writeObject(this);
        }
     }
    
     public void obtenerPartida() throws ClassNotFoundException{
         ObjectInputStream entrada;
         Partida partida;
                try{
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new java.io.File("."));
                chooser.setDialogTitle("Titulo");
                chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    System.out.println("Directorio: " + chooser.getCurrentDirectory());
                } else {
                    System.out.println("No seleccion ");
                }
                File f = chooser.getSelectedFile();
                FileInputStream fis = new FileInputStream(f);
                entrada = new ObjectInputStream(fis);
                partida = (Partida) entrada.readObject();
                }catch(IOException io){
                    System.out.println("No se puede"+io.getMessage());
                }
     }
    public static void main (String args[]) throws IOException, ClassNotFoundException{
        Partida p = new Partida();
        p.agregar(new Movimiento(1, new Casilla('a', 5)));
        p.agregar(new Movimiento(1, new Casilla('b', 6)));
        p.guardar();
        p.obtenerPartida();
        
    }
}
