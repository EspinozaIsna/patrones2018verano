package HistorialJuego;

import casilla.Casilla;
import java.io.Serializable; 

public class Movimiento implements Serializable{
    Integer dimension;
    Casilla posActual;
    
    public Movimiento(Integer dimension, Casilla posActual){
        this.dimension = dimension;
        this.posActual = posActual;
    }

    public Integer getDimension() {
        return dimension;
    }

    public Casilla getPosActual() {
        return posActual;
    }
}
