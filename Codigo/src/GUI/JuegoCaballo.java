package GUI;

import casilla.Casilla;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import piezas.Caballo;
import tablero.Tablero;

public class JuegoCaballo extends JFrame implements ActionListener {
    private JButton incrementar,decrementar,deshacer,rehacer,vHistorial;//,deshacerMovimiento,rehacerMovimiento,verHistorial;
    private Tablero tablero;
    private JPanel vistaTablero;
    private Caballo caballo ;
    private JLabel casillas[];
    private JTextArea console;
    
    public JuegoCaballo(Integer dimension,Character columna,Integer fila) {
        tablero = new Tablero(dimension);
        caballo = new Caballo(tablero,new Casilla(columna, fila));
        caballo.addObserver(tablero);
        initComponents();
        
    }

    private void initComponents() {
        
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        this.setLayout(new BorderLayout());
        JPanel historial = creaHistorial();
        vistaTablero = new JPanel();
        vistaTablero.setPreferredSize(new Dimension(400,400));
        actualizarTablero();
        JPanel menu = creaMenu();
   
        console = new JTextArea(5, 20);
        console.setEditable(false);

        
               
        this.add(menu, BorderLayout.NORTH);
        this.add(historial, BorderLayout.WEST);
        this.add(console,BorderLayout.SOUTH);
        this.add(vistaTablero, BorderLayout.CENTER);
        
        this.setSize(600, 600);
        this.setVisible(true);
    }
    
    private JPanel creaHistorial(){
        JPanel panel = new JPanel();
        JPanel btns = new JPanel();
        btns.setPreferredSize(new Dimension(100,200));
        deshacer = new JButton("Deshacer");
        rehacer = new JButton("Rehacer");
        vHistorial = new JButton("Mostral Historial");
        deshacer.addActionListener(this);
        rehacer.addActionListener(this);
        vHistorial.addActionListener(this);
        btns.add(deshacer);
        btns.add(rehacer);
        btns.add(vHistorial);
        
        panel.setLayout(new BorderLayout());
        
        panel.add(new JLabel("Historial de Movimientos"),BorderLayout.NORTH);
        panel.add(btns,BorderLayout.CENTER);
       
        return panel;
    }
    private JPanel creaMenu(){
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(100,50));
        incrementar = new JButton("Incrmentar");
        incrementar.addActionListener(this);
        decrementar = new JButton("decrementar");
        decrementar.addActionListener(this);
        
        panel.add(incrementar);
        panel.add(decrementar);
        return panel;
    }
    public void actualizarTablero(){
        vistaTablero.removeAll();
        vistaTablero.updateUI();
        vistaTablero.repaint();
        
        casillas = new JLabel[this.tablero.tamaño() * this.tablero.tamaño()];
        Integer i = 0;
        Iterator<Casilla> it = tablero.getIterador();
        vistaTablero.setLayout(new GridLayout(tablero.tamaño(), tablero.tamaño()));
        Casilla casilla;
        
        while(it.hasNext()){
                casilla = it.next();
                casillas[i] = new JLabel();
                casillas[i].addMouseListener( new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent ev){
                        JLabel label = (JLabel)ev.getSource();
                        String notacionAlgraica = label.getText();
                        Casilla casillaDestino = new Casilla(notacionAlgraica.charAt(0), Integer.parseInt(notacionAlgraica.substring(1, notacionAlgraica.length())));
                        if(!caballo.moverA(casillaDestino))
                            JOptionPane.showMessageDialog(null, "Movimiento invalido");
                        actualizarTablero();
                        
                    }
                });
                
                if(casilla.equals(caballo.posicionActual()))
                    casillas[i].setText(caballo.imagen());
                else
                    casillas[i].setText(casilla.notacionAlgebraica());
                                
                casillas[i].setHorizontalAlignment(JLabel.CENTER);
                casillas[i].setVerticalAlignment(JLabel.CENTER);
                casillas[i].setPreferredSize(new Dimension(40,40));
                casillas[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                if (casilla.getFila()% 2 == 0) {
                    if(casilla.getColumna()% 2 != 0){
                        casillas[i].setBackground(new Color(172, 94, 0));
                        casillas[i].setOpaque(true);
                    }else{
                        casillas[i].setBackground(new Color(213, 178, 120));
                        casillas[i].setOpaque(true);
                    }
                }else{
                    if(casilla.getColumna()% 2 == 0){
                        casillas[i].setBackground(new Color(172, 94, 0));
                        casillas[i].setOpaque(true);
                    }else{
                        casillas[i].setBackground(new Color(213, 178, 120));
                        casillas[i].setOpaque(true);
                    }
                }
                if(caballo.existeCasilla(casilla)){
                    casillas[i].setBackground(new Color(10, 101, 201));
                        casillas[i].setOpaque(true);
                }
                vistaTablero.add(casillas[i]);
            i++;
        }
        //if(tablero.)
        
    }    

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        if(button == decrementar){
            if(tablero.decrementarDimension(1,caballo.posicionActual())){
                caballo.actualizaTablero(tablero);
                actualizarTablero();
                this.repaint();
            }
        }
        if(button == incrementar){
            tablero.incrementarDimension(1);
            caballo.actualizaTablero(tablero);
            actualizarTablero();
            this.repaint();
        }
        if(button == deshacer){
            System.out.println("Deshacer activado");
            caballo.deshacerMovimiento();
            actualizarTablero();
            this.repaint();
        }
        if(button == rehacer){
            caballo.rehacerMovimiento();
            //JOptionPane.showMessageDialog(null, "NO hay mas movimientos por rehacer");
            actualizarTablero();
            this.repaint();
        }
        if(button == vHistorial){
            console.removeAll();
           console.setText(caballo.toString());
            this.repaint();
        }
        
               
    }
}
