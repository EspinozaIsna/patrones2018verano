package piezas;

import casilla.Casilla;
import java.util.Iterator; 

public interface Pieza{
    public Boolean moverA(Casilla casilla);
    public void deshacerMovimiento();
    public void rehacerMovimiento();
    public Iterator getHistorialMovimientos();
    public String imagen();
    public String toString();
    
}
