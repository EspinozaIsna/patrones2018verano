
package piezas;

import casilla.Casilla;
import java.util.Iterator;
import java.util.Observable;
import java.util.Stack;
import tablero.Tablero;
import java.io.Serializable;
        
public class Caballo  extends Observable implements Pieza, Serializable{
    private final Stack<Casilla> historialMovimientos;
    private final Stack<Casilla> backupMovimientos;
    private Casilla casillaActual;
    private Tablero tablero;

    public Caballo(Tablero tablero,Casilla casillaInicio){
        this.historialMovimientos = new Stack();
        this.backupMovimientos = new Stack();
        
        if(tablero.existeCasilla(casillaInicio)){
            historialMovimientos.push(casillaInicio);
            this.casillaActual = casillaInicio;
            this.tablero = tablero;
        }else{
            
            this.casillaActual = new Casilla('a',1);
        }
        this.notificar();
    }
    
    public void actualizaTablero(Tablero tablero){
        this.tablero = tablero;
        Iterator<Casilla> iterator = historialMovimientos.iterator();
        Casilla casilla;
        while(iterator.hasNext()){
            casilla = iterator.next();
            if(!tablero.existeCasilla(casilla))
                iterator.remove();
        }
    }
    public Casilla posicionActual(){
        return (Casilla)historialMovimientos.peek();
    }
    
    @Override
    public Iterator getHistorialMovimientos(){
        return historialMovimientos.iterator();
    }
    
    public Boolean existeCasilla(Casilla casilla){
        return historialMovimientos.contains(casilla);
    }
    
    private void notificar(){
        this.setChanged();
        this.notifyObservers(this.casillaActual);
    }
    
    @Override
    public Boolean moverA(Casilla destino){
        if(!backupMovimientos.empty())
            backupMovimientos.clear();
        
        if(validarMovimientos(destino)){
            historialMovimientos.add(destino);
            this.casillaActual = posicionActual();
            this.notificar();
            return true;
        }else
            return false;
    }
    
    @Override
    public void deshacerMovimiento(){
        if(historialMovimientos.size() > 1){
            backupMovimientos.push(historialMovimientos.pop());
            Casilla destino = historialMovimientos.pop();
            if(!validarMovimientos(destino)){
                historialMovimientos.push(destino);
            }else{
                historialMovimientos.push(destino);
                historialMovimientos.push(backupMovimientos.pop());
            }
            this.casillaActual = this.posicionActual();
            this.notificar();
        }      
    }
    
    @Override
    public void rehacerMovimiento(){
        if(!backupMovimientos.isEmpty()){
            Casilla destino = backupMovimientos.pop();
            if(!validarMovimientos(destino)){
                historialMovimientos.push(destino);
            }
            else{
                backupMovimientos.clear();
            }
            this.casillaActual = posicionActual();
            this.notificar();
        }    
    }
   
    private Boolean validarMovimientos(Casilla casillaDestino){
        if(this.tablero.existeCasilla(casillaDestino) && casillaDestino.getPieza() instanceof PiezaNula && !historialMovimientos.contains(casillaDestino)){
            if((casillaActual.casillasDerecha(1).casillasArriba(2)).equals(casillaDestino))        
                return true;
                   
            if(casillaActual.casillasDerecha(2).casillasArriba(1).equals(casillaDestino) )
                return true;
            
            if(casillaActual.casillasDerecha(2).casillasAbajo(1).equals(casillaDestino) )
                return true;
            
            if(casillaActual.casillasDerecha(1).casillasAbajo(2).equals(casillaDestino))
                return true;
            
            if(casillaActual.casillasIzquierda(2).casillasAbajo(1).equals(casillaDestino))
                return true;
            
            if(casillaActual.casillasIzquierda(1).casillasAbajo(2).equals(casillaDestino))
                return true;
            
            if(casillaActual.casillasIzquierda(2).casillasArriba(1).equals(casillaDestino))
                return true;
            
            return casillaActual.casillasIzquierda(1).casillasArriba(2).equals(casillaDestino);
        }
        return false;
    } 
    
    @Override
    public String imagen(){
        return "♞";
    }

           
    @Override
    public String toString(){
       String s;
       s = "Nombre :: {Caballo},"+
               "\nPosición Actual :: {"+posicionActual()+"},"+
               "\nhistorial :: { "+ historialMovimientos.toString()+ " }";
       return s;
   }

}
