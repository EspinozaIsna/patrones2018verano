package casilla;

import java.util.Objects;
import piezas.Caballo;
import piezas.Pieza;
import piezas.PiezaNula;
import java.io.Serializable; 

public class Casilla implements Comparable, Serializable{
   private Pieza pieza;
   private Integer fila;
   private Integer columna;

    public Casilla(Character columna, Integer fila) {
        if(Character.isLowerCase(Character.toLowerCase(columna)) && fila > 0 && fila < 27){
            this.columna = (int)(columna);
            this.fila = fila; 
            this.pieza = new PiezaNula();
        }else{
            this.columna = 164;
            this.fila = 5;
            this.pieza = new PiezaNula();
        }   
    }

    public Integer getColumna() {
        return this.columna;
    }
    
    public Integer getFila() {
        return this.fila;
    } 
    
    public Casilla casillasDerecha(Integer indice){
        return new Casilla((char)(this.columna+indice), this.fila);
    }
    
    public Casilla casillasIzquierda(Integer indice){
        return new Casilla((char)(this.columna-indice), this.fila);
    }
    public Casilla casillasAbajo(Integer indice){
        return new Casilla((char)(this.columna.intValue()), (this.fila-indice));
    }
    public Casilla casillasArriba(Integer indice){
        return new Casilla((char)(this.columna.intValue()), (this.fila+indice));
    }
    public void incrementarPosicion(Integer fila,Integer columna){
        if((this.columna+columna) < 124 && (this.fila+fila) < 27){
            this.fila += fila;
            this.columna += columna;
        }
    }
    
    public void decrementarPosicion(Integer fila,Integer columna){
        if((this.columna-columna) > 96 && (this.fila-fila) > 0){
            this.columna -= columna;
            this.fila -= fila;
        }
    }
    public Boolean colocarPieza(Caballo pieza){
        this.pieza = pieza;
        return true;
    }
    
    public Pieza getPieza(){
        return this.pieza;
    }
    
    public String notacionAlgebraica(){
        String s = ""+(char)this.columna.intValue()+this.fila;
        return s;
    }
    
   @Override
    public String toString(){
        String s = "";
        if(!(pieza instanceof PiezaNula))
            s += "Pieza :: {Caballo}";
        
        s += "Casilla :: {"+this.notacionAlgebraica()+"}";
        return s;
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null ) return false;
        if ( this == obj ) return true;
        if ( ! (obj instanceof Casilla ) ) return false;
        Casilla c = (Casilla) obj;
        return (this.notacionAlgebraica().equalsIgnoreCase(c.notacionAlgebraica())) ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.fila);
        hash = 79 * hash + Objects.hashCode(this.columna);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        Casilla casilla = (Casilla)o;
       return this.notacionAlgebraica().compareTo(casilla.notacionAlgebraica());
    }

  

   
    
}
